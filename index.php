<?php

// Kickstart the framework
$f3=require('lib/base.php');

$f3->set('DEBUG',3);
if ((float)PCRE_VERSION<7.9) {
    trigger_error( 'PCRE version is out of date' );
}

// Load configuration
$f3->config('config/site.cfg');

$f3->route('GET /',
	function($f3) {
		$f3->set('content','index.html');
		echo Template::instance()->render('template/site.html');
	}
);

$f3->run();
